php-laravel-framework (8.83.26+dfsg-2) unstable; urgency=medium

  * Source only upload for migration to testing

 -- Robin Gustafsson <robin@rgson.se>  Fri, 11 Nov 2022 23:09:03 +0100

php-laravel-framework (8.83.26+dfsg-1) unstable; urgency=medium

  [ Robin Gustafsson ]
  * New upstream version 8.83.26+dfsg
    - Fix compatibility with Symfony 5 (Closes: #1000560)
  * Fix capitalization error in description
  * Update standards version to 4.6.1, no changes needed.
  * Patch egulias/email-validator v3 compatibility
  * Patch voku/portable-ascii v2 compatibility
  * Avoid extra composer.json files at build
  * Use phpabtpl to generate autoloaders
  * Install an autoloader override for phpabtpl
  * Add superficial tests to load the library code

  [ Katharina Drexel ]
  * Adding more laravel components in control file.
    - New packages: php-illuminate-collections, php-illuminate-macroable,
      php-illuminate-testing
  * Completing missing packages in autoload.tpl.
  * Removing obsolete patches.
  * Correcting lintian-overrides statement.
  * Cosmetics in control file.

 -- Robin Gustafsson <robin@rgson.se>  Wed, 09 Nov 2022 21:51:45 +0100

php-laravel-framework (6.20.14+dfsg-3) unstable; urgency=high

  * Fix security issue: XSS vulnerability in the Blade templating engine
    (CVE-2021-43808, Closes: #1001333)
  * Fix security issue: Failure to block the upload of executable PHP content
    (CVE-2021-43617, Closes: #1002728)
  * Rename main branch to debian/latest (DEP-14)
  * Update lintian override to php-markdown
  * Bump Standards-Version

 -- Robin Gustafsson <robin@rgson.se>  Tue, 28 Dec 2021 16:18:01 +0100

php-laravel-framework (6.20.14+dfsg-2) unstable; urgency=medium

  * Fix security issue: SQL injection with Microsoft SQL Server
    (Closes: #987831)

 -- Robin Gustafsson <robin@rgson.se>  Fri, 30 Apr 2021 18:23:38 +0200

php-laravel-framework (6.20.14+dfsg-1) unstable; urgency=medium

  * New upstream version 6.20.14+dfsg
    - Fix security issue: More unexpected bindings in QueryBuilder
  * Replace git attributes with uscan's gitexport=all

 -- Robin Gustafsson <robin@rgson.se>  Fri, 22 Jan 2021 18:39:34 +0100

php-laravel-framework (6.20.11+dfsg-1) unstable; urgency=medium

  * Set upstream metadata fields: Security-Contact.
  * New upstream version 6.20.11+dfsg
    - Fix security issue: Unexpected bindings in QueryBuilder
      (CVE-2021-21263, Closes: #980095)
  * Bump Standards-Version

 -- Robin Gustafsson <robin@rgson.se>  Fri, 15 Jan 2021 00:35:41 +0100

php-laravel-framework (6.20.6+dfsg-1) unstable; urgency=medium

  [ Robin Gustafsson ]
  * New upstream version 6.20.6+dfsg
  * Remove Salsa CI config

  [ David Prévot ]
  * Update generate-autoload-tpl for php-doctrine-inflector (>= 2)
    (Closes: #976799)

 -- Robin Gustafsson <robin@rgson.se>  Sun, 06 Dec 2020 16:40:52 +0100

php-laravel-framework (6.20.5+dfsg-1) unstable; urgency=medium

  * Initial release (Closes: #951159)
  * Merge php-illuminate-* source packages
    (Closes: #975304, #975306, #975307, #975308)

 -- Robin Gustafsson <robin@rgson.se>  Tue, 24 Nov 2020 20:32:22 +0100
